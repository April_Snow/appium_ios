package ru.gb.locators.iOS;

import org.openqa.selenium.By;
import ru.gb.locators.interfaces.LoginPageLocators;

public class iOSLoginPageLocators implements LoginPageLocators {
    @Override
    public By emailInput() {
        return null;
    }

    @Override
    public By loginButton() {
        return null;
    }

    @Override
    public By errorEmptyPasswordInputMessage() {
        return null;
    }

    @Override
    public By passwordInput() {
        return null;
    }

    @Override
    public By errorEmptyEmailMessage() {
        return null;
    }
}
