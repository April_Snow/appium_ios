package ru.gb.locators.interfaces;


import org.openqa.selenium.By;

public interface LoginPageLocators {

    By emailInput ();

    By loginButton();

    By errorEmptyPasswordInputMessage();

    By passwordInput();

    By errorEmptyEmailMessage();

}
