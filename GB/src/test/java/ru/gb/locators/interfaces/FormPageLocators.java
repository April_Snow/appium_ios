package ru.gb.locators.interfaces;

import org.openqa.selenium.By;

public interface FormPageLocators {

    By fieldInput();

    By fieldResultInput();

    By switchButton();

    By switchMessage();

}
