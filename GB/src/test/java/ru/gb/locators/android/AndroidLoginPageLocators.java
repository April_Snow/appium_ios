package ru.gb.locators.android;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import ru.gb.locators.interfaces.LoginPageLocators;

public class AndroidLoginPageLocators implements LoginPageLocators {

   public By emailInput (){
      return MobileBy.AccessibilityId("input-email");
   }
   public By loginButton() {
      return MobileBy.xpath("//android.view.ViewGroup[@content-desc=\"button-LOGIN\"]/android.view.ViewGroup");
   }
   public By errorEmptyPasswordInputMessage(){
      return MobileBy.xpath("//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");
   }

   public By passwordInput(){
      return MobileBy.AccessibilityId("input-password");
   }

   public By errorEmptyEmailMessage() {
      return MobileBy.xpath("//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[1]");
   }

}
