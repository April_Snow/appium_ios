package ru.gb.locators.android;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import ru.gb.locators.interfaces.MainPageLocators;

public class AndroidMainPageLocators implements MainPageLocators {

    public By menuLoginButton (){
      return  MobileBy.xpath("//android.view.ViewGroup[@content-desc=\"Login\"]/android.view.ViewGroup/android.widget.TextView");
    }

    public By menuFormButton(){
        return MobileBy.xpath("//android.view.ViewGroup[@content-desc=\\\"Forms\\\"]/android.view.ViewGroup/android.widget.TextView");
    }

}
