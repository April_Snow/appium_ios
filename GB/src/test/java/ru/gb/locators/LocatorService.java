package ru.gb.locators;

import ru.gb.locators.android.AndroidFormPageLocators;
import ru.gb.locators.android.AndroidLoginPageLocators;
import ru.gb.locators.android.AndroidMainPageLocators;
import ru.gb.locators.iOS.iOSFormPageLocators;
import ru.gb.locators.iOS.iOSLoginPageLocators;
import ru.gb.locators.iOS.iOSMainPageLocators;
import ru.gb.locators.interfaces.FormPageLocators;
import ru.gb.locators.interfaces.LoginPageLocators;
import ru.gb.locators.interfaces.MainPageLocators;

public class LocatorService {

        public static final MainPageLocators MAIN_PAGE_LOCATORS = System.getProperty("platform")
                .equals("Android") ? new AndroidMainPageLocators() : new iOSMainPageLocators();

        public static final LoginPageLocators LOGIN_PAGE_LOCATORS = System.getProperty("platform")
                .equals("Android") ? new AndroidLoginPageLocators() : new iOSLoginPageLocators();

    public static final FormPageLocators FORM_PAGE_LOCATORS = System.getProperty("platform")
            .equals("Android") ? new AndroidFormPageLocators() : new iOSFormPageLocators();
}
