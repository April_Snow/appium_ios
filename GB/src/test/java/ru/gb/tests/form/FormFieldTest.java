package ru.gb.tests.form;

import org.testng.annotations.Test;
import ru.gb.base.BaseTest;

public class FormFieldTest extends BaseTest {

    public static final String TEXT= "HelpMe";

    @Test
    public void checkField(){
        openApp()
                .clickMenuFormButton()
                .writeText(TEXT)
                .checkWhatTextRight(TEXT);
    }
}
