package ru.gb.tests.form;

import org.testng.annotations.Test;
import ru.gb.base.BaseTest;

public class FormSwitchTest extends BaseTest {

    public static final String SWITCH_TEXT= "Click to turn the switch OFF";

    @Test
    public void checkSwitch(){
        openApp()
                .clickMenuFormButton()
                .clickSwitchButton()
                .checkSwitchText(SWITCH_TEXT);
    }
}
