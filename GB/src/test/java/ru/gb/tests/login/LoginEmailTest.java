package ru.gb.tests.login;

import org.testng.annotations.Test;
import ru.gb.base.BaseTest;

public class LoginEmailTest extends BaseTest {

    public static final String PASSWORD = "kukusiki" ;
    public static final String EXPECTED_EMPTY_EMAIL_TEXT = "Please enter a valid email address";

    @Test
    public void checkEmptyEmail(){
        openApp()
                .clickLoginMenuButton()
                .setPassword(PASSWORD)
                .clickLoginButton()
                .checkErrorEmptyEmailInputMessage(EXPECTED_EMPTY_EMAIL_TEXT);
    }
}
