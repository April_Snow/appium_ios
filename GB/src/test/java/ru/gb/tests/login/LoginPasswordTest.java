package ru.gb.tests.login;
import org.testng.annotations.Test;
import ru.gb.base.BaseTest;

public class LoginPasswordTest extends BaseTest {

    public static final String EMAIL = "we@list.ru" ;
    public static final String EXPECTED = "Please enter at least 8 characters";

    @Test
    public void checkEmptyPassword() {

        openApp()
                .clickLoginMenuButton()
                .setEmail(EMAIL)
                .clickLoginButton()
                .checkErrorEmptyPasswordInputMessage(EXPECTED);

    }

}


